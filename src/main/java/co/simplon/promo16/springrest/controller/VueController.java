package co.simplon.promo16.springrest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class VueController {
    
    @GetMapping("/first-vue")
    public String firstVue() {
        return "first-vue";
    }

    @GetMapping("/vue-list-person")
    public String vueListPerson() {
        return "vue-list-person";
    }
    @GetMapping("/vue-search")
    public String vueSearch() {
        return "vue-search";
    }
}

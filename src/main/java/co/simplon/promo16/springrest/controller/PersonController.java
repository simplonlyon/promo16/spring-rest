package co.simplon.promo16.springrest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.promo16.springrest.PersonRepository;
import co.simplon.promo16.springrest.entity.Person;

/**
 * Un contrôleur rest permettra de servir des données pures sous forme de de JSON qui 
 * pourront ensuite être requêtées par des applications front ou back.
 * Pour y accéder, il faudra faire des requêtes HTTP en GET/POST/PATCH/PUT/DELETE selon
 * le type d'action que l'on souhaite réaliser sur l'url (voir plus bas pour la structure
 * des url communément utilisé)
 */
@RestController
@RequestMapping("/api/person")
public class PersonController {

    @Autowired
    private PersonRepository repo;


    @GetMapping
    public List<Person> listPerson(@RequestParam(required = false) String search) {
        if(search != null) {
            return repo.findBySearch(search);
        }
        return repo.findAll();
    }

    @PostMapping
    public Person addPerson(@RequestBody Person person) {
        repo.save(person);
        return person;
    }

    // GET /api/person -> renvoie toutes les personnes
    // GET /api/person/id -> renvoie la personne spécifique avec cet id
    // POST /api/person -> ajouter une personne
    // DELETE /api/person/id -> supprimer la personne avec l'id
    // PATCH ou PUT /api/person/id -> mets à jour une personne selon son id

    
}

package co.simplon.promo16.springrest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.springrest.entity.Person;

@Repository
public class PersonRepository {

    @Autowired
    private DataSource dataSource;

    public List<Person> findAll() {
        List<Person> list = new ArrayList<>();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM person");
                    
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(
                        new Person(
                                result.getInt("id"),
                                result.getString("name"),
                                result.getString("firstname"),
                                result.getInt("age"))
                        );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    public List<Person> findBySearch(String search) {
        List<Person> list = new ArrayList<>();
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("SELECT * FROM person WHERE firstname LIKE ? OR name LIKE ?");
            stmt.setString(1, "%"+search+"%");
            stmt.setString(2, "%"+search+"%");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                list.add(
                        new Person(
                                result.getInt("id"),
                                result.getString("name"),
                                result.getString("firstname"),
                                result.getInt("age"))
                        );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }
    
    public void save(Person person) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO person (name,firstname,age) VALUES (?,?,?)");
                    stmt.setString(1, person.getName());
                    stmt.setString(2, person.getFirstname());
                    stmt.setInt(3, person.getAge());
            stmt.executeUpdate();
           
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
